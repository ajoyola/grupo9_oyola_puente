#import <stdio.h>
#import <Foundation/Foundation.h>
#import "Tarjeta.h"

@implementation Tarjeta
	
	-(id) init {
		self = [super init];
			if(self){
				
				contrasena= [[NSMutableArray alloc] init];
				idT=[NSString stringWithFormat: @"%05d", 1]; //ID = 00001
				monto=0;
			}
			return self;
	}
	-(NSMutableArray *) getContrasena{ return contrasena;}
	-(void) setContrasena: (NSMutableArray *) cContr{
		contrasena= cContr;
	}
	-(void) print{
		printf("\nID Tarjeta: %s",[idT cString]);
		printf("\nCONTRASENA: ");
		for(NSString* item in contrasena){
			printf ("%s ",[item cString]);
		}
	}
	
	- (NSString *) getID{ return idT; }
	- (void) setMonto : (int) m{
		monto=m;
	}
	- (void) setID : (NSString * ) i{
		idT=i;
	}
	-(int) getMonto{return monto;}
	-(void) AumentarID: (int) i{
		int a= [idT intValue]+i;
		idT= [NSString stringWithFormat:@"%05d",a];		
	}

@end
