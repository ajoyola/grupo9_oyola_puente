#include <stdio.h>
#import <Foundation/Foundation.h>
#import "Persona.h"

@interface Programa: NSObject{}
	+(NSMutableArray*) clientes;

@end

@implementation Programa
	+(NSMutableArray*) clientes{
		Cliente * c1 = [Persona init];
		[c1 setNombre:@"Maria Alejandra"];
		NSMutableArray * list= [NSMutableArray new];
		[list addObject:c1];
		return list
		
	}
@end


int main (void){
	id pool = [NSAutoreleasePool new];
	
	NSMutableArray *lista = [Programa clientes];
	for(Cliente *item in lista ){
		NSLog(@"%@",[item getNombre]);
	}
	
	[pool release];
	return 0;
}