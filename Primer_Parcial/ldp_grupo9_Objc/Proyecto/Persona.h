#import <stdio.h>
#import <Foundation/Foundation.h>
#import "Tarjeta.h"


@interface Persona: NSObject {
	NSString * nombre,*apellido,*direccion;
	NSUInteger contrasena;
	Tarjeta * tcredit ;
}

-(NSString *) getNombre;
-(void) setNombre: (NSString*) n;
-(void) setApellido: (NSString *) a;
-(void) setContrasena: (NSUInteger) c;
-(void) print;
-(void) setPersona: (NSString *) n :(NSString *) a :(NSUInteger) c; 
-(NSUInteger) getContrasena;
-(id) init;
-(Tarjeta*) getTarjeta;
-(void) setTarjeta: (Tarjeta *) t;
@end
