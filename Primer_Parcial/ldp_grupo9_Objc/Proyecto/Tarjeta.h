#import <stdio.h>
#import <Foundation/Foundation.h>

@interface Tarjeta: NSObject {
	NSString * idT;
	NSMutableArray *contrasena;
	int monto;
	}
	
	- (id) init;
	- (NSMutableArray *) getContrasena;
	- (void) setContrasena: (NSMutableArray *) c; 
	- (void) print;
	- (int) getMonto;
	- (void) setMonto : (int) m;
	- (void) AumentarID: (int) i;
	- (NSString *) getID;
@end

