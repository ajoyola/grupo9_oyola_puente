#import <stdio.h>
#import <stdlib.h>
#import <Foundation/Foundation.h>
#import "Persona.h"
#import <time.h>
//#import "Tarjeta.h"

#define arc4random   rand
#define srandom  srand

void mostrar_Inicio();
void pedirID_part1(NSString *ID, NSString *correcta);
void pedirID_part2();
void pedirID_part3();
void pedirContrasena(NSMutableArray *temp);
void retiros_inicio();
void retiros_pedirValor(NSString *valor, NSString *correcta);
void cambiarMascara(NSString*antigua, NSString *nueva);


///LLAMADA DE FUNCIONES
void limpiar();
NSMutableArray* clientes(NSMutableArray *temp1);
//NSArray* Generar_Clave(NSMutableArray *claves, NSUInteger cUser);
NSMutableArray* Tentativa_Claves(NSMutableArray* cadena, NSMutableArray* temp,int n, int M, int m);
int repetido (NSString *l, NSMutableArray* temp);
NSMutableArray *MascaraTemp (NSMutableArray *temp);
NSMutableArray *claveUsuario(NSMutableArray *temp);
NSMutableArray *claveNumericaUsuario();
NSMutableArray *Mascara(NSMutableArray *temp, NSMutableArray *cadPosUsua, NSMutableArray *clave);
void imprimirMascara(NSMutableArray *temp);
NSString *printMascaraPartes(NSMutableArray *temp, int a, int b);
NSString *convertirArray_String(NSMutableArray *clave);
NSString* convertirClave_String(NSMutableArray *clave);
int IDTarjeta_isValido(NSString *ID,NSMutableArray *list);

///Declarar Variables
NSMutableArray* MascaraContrasenaUsuario(NSMutableArray *clave,NSMutableArray *temp);
NSMutableArray* getContrasenaID_Tarj(NSString *ID,NSMutableArray *list);
int retirar(int monto, NSString *ID, NSMutableArray *list);
NSString* nuevaClaveTarjeta(NSString *ID, NSMutableArray *list);


int main(int argc, const char *argv[])
{
	id pool = [[NSAutoreleasePool alloc] init];
	int size,tarjVal=-1,op=-1, i,j,k,count=0,count1=0,posUsuario,posCadena,rep,pos,pos1,posTemp;
	int inicio,flag,flag1,flag2,opc,opc1,opc2,opc3,nuevoMonto;
	NSMutableArray *temp2=[[NSMutableArray alloc] init];
	NSMutableArray *list= clientes(temp2);
	NSString *IDTarjeta,*contrasena,*contrasena1,*opcion,*opcion1,*opcion2,*opcion3;
	NSMutableArray *clave,*posNumerica,*temp,*temp1,*temp3,*claveTarjeta;
	char ID[5] = {0};
	char key[7] = {0};
	char OP[3] = {0};
	char OP1[3] = {0};
	char OP2[3] = {0};
	char retiro[6] = {0};
	//srand(time(NULL));
	inicio=0;
	while(inicio==0){
		limpiar();
		flag=0;flag1=0;flag2=0;
		tarjVal=0;
		mostrar_Inicio();
		
		while (tarjVal!=1){//VALIDACION ID-TARJETA
			pedirID_part1(@"    ", @"                  "); //Metodo para mostrar cuando se pide ID_Tarjeta
			printf("\n_____________________________");
			printf("\n\nINGRESE ID TARJETA: ");
			scanf("%s",ID);
			getch();
			limpiar();
			IDTarjeta= [NSString stringWithUTF8String:ID];
			tarjVal=IDTarjeta_isValido(IDTarjeta,list);
			
			if(tarjVal==0){
				pedirID_part1(IDTarjeta,@"ID Tarjeta incorrecta" );
				inicio=0;
				getch();
				limpiar();
			}
			else if (tarjVal==1){
				pedirID_part1(IDTarjeta,@"ID Tarjeta es correcta" );
				flag=0;
				flag1=0;
				flag2=0;
				getch();
				limpiar();
		
				flag1=0;
				while(flag<3){ ////INGRESE CONTRASE�A
					clave = getContrasenaID_Tarj(IDTarjeta,list);
					NSMutableArray *array= [[NSMutableArray alloc] init];
					array= clave;
					temp1=MascaraTemp(array);
					list= clientes([[NSMutableArray alloc] init]);
					
					temp3= [[NSMutableArray alloc] init];
					for(i=4;i<=33;i++){
						[temp3 addObject:[temp1 objectAtIndex:i]];
					}
					NSMutableArray *array1= clave;
					posNumerica=MascaraContrasenaUsuario(array1,temp3);
					
					contrasena= convertirArray_String(posNumerica);
					flag1=0;
					while(flag1==0){ //VALIDAR CONTRASE�A

						printf("\nIngrese la clave: ");
						scanf("%s",key);
						contrasena1= [NSString stringWithUTF8String:key];
						if([contrasena isEqualToString:contrasena1]!=1){
							printf("\nContrasena incorrecta, intente una vez mas\n");
							flag1=1;
							flag++;
						}
						else {
							printf("\nContrasena correcta\n");
							flag1=1;
							flag2=0;
							while(flag2==0){
								retiros_inicio();
								printf("\nIngrese la opcion: ");
								scanf("%s",OP);
								opcion= [NSString stringWithUTF8String:OP];
								opc= [opcion intValue];

								getch();
								limpiar();
								switch (opc){
									case 3: //PARTE RETIROS
										retiros_pedirValor(@"    ", @"                  ");
										printf("\nCantidad: ");
										scanf("%s",retiro);
										opcion3= [NSString stringWithUTF8String:retiro];
										opc3= [opcion3 intValue];
										nuevoMonto= retirar(opc3, IDTarjeta, list);
										getch();
										limpiar();
										
										//PANTALLA MOSTRANDO EL VALOR DEL RETIRO
										retiros_pedirValor(opcion3,[NSString stringWithFormat:@"Su saldo es: %d   ",nuevoMonto]);
										printf("\nQue desea hacer (Cancelar/Aceptar)? ");
										scanf("%s",OP1);
										opcion1= [NSString stringWithUTF8String:OP1];
										opc1= [opcion1 intValue];
										switch (opc1){
											case 6:
												flag2=1;
												flag1=1;
												flag=3;
												inicio=0;
												limpiar();
												break;
										}
										break;
									case 4: //PARTE CAMBIO DE MASCARAS
										cambiarMascara(printMascaraPartes(clave,0,3),nuevaClaveTarjeta(IDTarjeta,list));
										printf("\nQue desea hacer (Cancelar/Aceptar)? ");
										scanf("%s",OP2);
										opcion2= [NSString stringWithUTF8String:OP2];
										opc2= [opcion2 intValue];
										switch (opc2){
											case 6:
												flag2=1;
												flag1=1;
												flag=3;
												inicio=0;
												limpiar();
												break;
										}
										break;
										case 6:
											flag2=1;
											flag1=1;
											flag=3;
											inicio=0;
											limpiar();
											break;
								}
							}						
						}
					}
					
				}
				
				
			}
		}
		
	}

	[temp2 release];
	[clave release];
	[posNumerica release];
	[temp release];
	[temp1 release];
	[temp3 release];
	[claveTarjeta release];
	return 0;
	
}


void limpiar(){	system("CLS");} //LIMPIA LA PANtALLA

int IDTarjeta_isValido(NSString *ID,NSMutableArray *list){ //Metodo de Validacion de ID tarjeta
	int r=-1;
	for (Persona *item in list){
		Tarjeta *t=[item getTarjeta];
		if([[t getID] isEqualToString:ID]==1){
			NSMutableArray *clave=[t getContrasena];
			for (NSString *item in clave){	
				if([item length]==2)
					printf ("%s ",[item cString]);	
				if([item length]==1)
					printf ("%s  ", [item cString]);
			}
				return 1;
		}
		r=0;
	}
	return r;
}

int retirar(int monto, NSString *ID, NSMutableArray *list){ //Metodo que hace el procedimiento retirar
	int nuevoMonto;
	for(Persona *p in list){
		Tarjeta *t= [p getTarjeta];
		if([[t getID] isEqualToString:ID]==1){
			if ([t getMonto]>= monto){
				nuevoMonto= [t getMonto]-monto;
				[t setMonto:nuevoMonto];
			}
		
		}
	}
	return nuevoMonto;

}

NSMutableArray* getContrasenaID_Tarj(NSString *ID,NSMutableArray *list){ //Obtiener contrase�a dado los parametros 
	int r=-1;
	NSMutableArray *clave = [[NSMutableArray alloc] init];
	for (Persona *item in list){
		Tarjeta *t=[item getTarjeta];
		if([[t getID] isEqualToString:ID]==1){
			clave=[t getContrasena];			
			return clave;
		}
		r=0;
	}
	
	if(r==0){
		return nil;
	}
	[clave release];
}


NSMutableArray *MascaraContrasenaUsuario(NSMutableArray *clave,NSMutableArray *temp){
	
	
	NSMutableArray *cadPosUsua=[[NSMutableArray alloc] init];
	cadPosUsua=claveNumericaUsuario();	
	temp=Mascara(temp,cadPosUsua,clave);
	printf ("\n\n ");
	pedirContrasena(temp);
	return cadPosUsua;

}




NSMutableArray* Tentativa_Claves(NSMutableArray* cadena, NSMutableArray* temp,int n, int M, int m){ //GENERA una mascara de n conjuntos de letras de M par y m de un solo caracter
	int count=0,count1=0,r=0,r1=0,rep=-1;
	//NSArray *ASCII_Cons_May = [[NSArray alloc] initWithObjects:@"B",@"C",@"D",@"F",@"G",@"H",@"J",@"K",@"L",@"M",@"N",@"P",@"Q",@"R",@"S",@"T",@"V",@"W",@"X",@"Y",@"Z",nil];
	//NSArray *ASCII_Cons_Min = [[NSArray alloc] initWithObjects:@"b",@"c",@"d",@"f",@"g",@"h",@"j",@"k",@"l",@"m",@"n",@"p",@"q",@"r",@"s",@"t",@"v",@"w",@"x",@"y",@"z",nil];
	//NSArray *ASCII_Voc_May = [[NSArray alloc] initWithObjects:@"A",@"E",@"I",@"O",@"U",nil];
	//NSArray *ASCII_Voc_Min = [[NSArray alloc] initWithObjects:@"a",@"e",@"i",@"o",@"u",nil];

	
	count=0;
	count1=0;
	r=0;r1=0;
	NSString *a,*b;
	NSArray *ASCII_Cons_May = [[NSArray alloc] initWithObjects:@"B",@"C",@"D",@"F",@"G",@"H",@"J",@"K",@"L",@"M",@"N",@"P",@"Q",@"R",@"S",@"T",@"V",@"W",@"X",@"Y",@"Z",nil];
	NSArray *ASCII_May = [[NSArray alloc] initWithObjects:@"B",@"C",@"D",@"F",@"G",@"H",@"J",@"K",@"L",@"M",@"N",@"P",@"Q",@"R",@"S",@"T",@"V",@"W",@"X",@"Y",@"Z",@"A",@"E",@"I",@"O",@"U",nil];
	NSArray *ASCII_Cons_Min = [[NSArray alloc] initWithObjects:@"b",@"c",@"d",@"f",@"g",@"h",@"j",@"k",@"l",@"m",@"n",@"p",@"q",@"r",@"s",@"t",@"v",@"w",@"x",@"y",@"z",nil];
	NSArray *ASCII_Min = [[NSArray alloc] initWithObjects:@"b",@"c",@"d",@"f",@"g",@"h",@"j",@"k",@"l",@"m",@"n",@"p",@"q",@"r",@"s",@"t",@"v",@"w",@"x",@"y",@"z",@"a",@"e",@"i",@"o",@"u",nil];

	srand(time(NULL));
	while((count1+count)<n){		//Saber cuantos doble o individuales caracteres se necesitan
		
		r=rand()%3+1;
		r1=rand()%21;
		
		if((r==1) && (count1<=M)){ //Parte de doble caracteres
			a=[ASCII_Cons_May objectAtIndex:r1]; //Tomar un caracter con el r1 aleatorio
			r1=rand()%26;
			b=[a stringByAppendingString:[ASCII_Min objectAtIndex:r1]]; //Concatena el primer caracter con el segundo escogido aleatoriamente
			rep=repetido(b,temp);

			while(rep!=0){ //Verifico que no esten repetidos
				r1=rand()%21;
				a=[ASCII_Cons_May objectAtIndex:r1]; //Tomar un caracter con el r1 aleatorio
				r1=rand()%26;
				b=[a stringByAppendingString:[ASCII_Min objectAtIndex:r1]]; //Concatena el primer caracter con el segundo escogido aleatoriamente
				rep=repetido(b,temp);

			}

			count1++;

			[cadena addObject: b];
			[temp addObject: b];
		}
		
		if((r==2)&& (count<m)){  //Parte de un caracter
			r1=rand()%21;
			a=[ASCII_May objectAtIndex:r1];
			rep=repetido(a,temp);

			while(rep!=0){ //Verifico que no esten repetidos
				r1=rand()%21;
				a=[ASCII_May objectAtIndex:r1];
				rep=repetido(a,temp);

			}

			count++;
			[cadena addObject: a];
			[temp addObject: a];
		}
					
	}
	count =0; count1=0; r=0;r1=0;
	//NSArray *clave = [[NSArray alloc] initWithObjects:@"a", @"Jk",nil];
	
	return cadena;
}

int repetido (NSString *l, NSMutableArray* temp){ //Verifica si las combinaciones de caracteres se encientra repetido en una lista
	int r=-1,r1=-1;
	if([temp count]==0){
		r=0;
	}
	else{
	
		for(NSString *item in temp){
			if ([l isEqualToString:item]==1) {
				r=1;
				return 1;
			}
			else{
				r=0;
				//return 0;
				}
		}
	}
	return r;
}

NSMutableArray *MascaraTemp (NSMutableArray *temp){ //Mascara temporal que genera las 6 m�scaras sin las del usuario agregadas
	int i,j;

	for (i=0; i< 3;i++){
		for (j=0; j< 2;j++){
			NSMutableArray *cadena=[[NSMutableArray alloc] init];
			cadena = Tentativa_Claves(cadena,temp,5,3,2);

			[cadena removeAllObjects ];
			[cadena release];
		}

	}
	return temp;
	
}

NSMutableArray *claveUsuario(NSMutableArray *temp){
	//printf ("\nCLAVE USUARIO\n ");
	
	NSMutableArray *clave=[[NSMutableArray alloc] init];
	clave = Tentativa_Claves(clave,temp,4,3,1);
	/*for (NSString *item in clave){	
		if([item length]==2)
			printf ("%s ",[item cString]);	
		if([item length]==1)
			printf ("%s  ", [item cString]);
	}*/
	return clave;

}

NSMutableArray *claveNumericaUsuario(){ //Genera las 4 posicion aleatoria de la mascara

	int i,posUsuario,rep;
	srand(time(NULL));
	NSMutableArray *cadPosUsua=[[NSMutableArray alloc] init];
	NSString* c;
	for (i=0; i< 4;i++){
		posUsuario=rand()%6+1;
		c= [NSString stringWithFormat:@"%i",posUsuario];			
		rep=repetido(c,cadPosUsua);

		while(rep!=0){ //Verifico que no esten repetidos
			posUsuario=rand()%6+1;
			c= [NSString stringWithFormat:@"%i",posUsuario];
			rep=repetido(c,cadPosUsua);

		}
		[cadPosUsua addObject:c];
			
	}

	return cadPosUsua;
}

NSMutableArray *Mascara(NSMutableArray *temp, NSMutableArray *cadPosUsua, NSMutableArray *clave){ //Genero la m�scara con las respectivas posiciones de la clave alfabetica del usuario
	int count1=0,pos,pos1,posTemp;
	
	for(NSString* item in cadPosUsua){
		pos = [item intValue];
		posTemp=rand()%5+1;
		//printf("rand: %d",posTemp);		
		pos1=posTemp;
		if(pos==1)
			posTemp= pos1-1;
		if(pos==2)
			posTemp= pos1+5-1;
		if(pos==3)
			posTemp= pos1+10-1;
		if(pos==4)
			posTemp= pos1+15-1;
		if(pos==5)
			posTemp= pos1+20-1;
		if(pos==6)
			posTemp= pos1+25-1;
		
		[temp replaceObjectAtIndex:posTemp withObject: [clave objectAtIndex:count1] ];
		count1++;
	}
	return temp;
}

void imprimirMascara(NSMutableArray *temp){ //Imprime una mascara temporal -- Sin dise�o
	int i,j,count=0,count1=0;

	count=0;
	count1=1;
	for (i=0; i< 3;i++){
		for (j=0; j< 2;j++){			
			count++;			
			if(j==0)
				printf ("[BOTON %d]  ",count);
			
			while((count1%5)!=0){
			//for (k=1; k<= 30;k++){
				
				NSString *item=[temp objectAtIndex:count1-1];	
				if([item length]==2)
					printf ("%s ",[item cString]);	
				if([item length]==1)
					printf ("%s  ", [item cString]);
				count1++;
			}
			if(count1<=30){
				NSString *item=[temp objectAtIndex:count1-1];
				if([item length]==2)
					printf ("%s ",[item cString]);	
				if([item length]==1)
					printf ("%s  ", [item cString]);
			}			
			count1++;
			if(j==1)
				printf ("  [BOTON %d]",count);
			printf ("\t ");
		}
		printf ("\n ");
	}
	
	}

void imprimirMascaraGrafica(NSMutableArray *temp){
	int i,j,count=0,count1=0;

	count=0;
	count1=6;
	for (i=0; i< 3;i++){
		for (j=0; j< 2;j++){			
			count++;			
			if(j==0)
				printf ("[BOTON %d]  ",count);
			
			while((count1%5)!=0){
			//for (k=1; k<= 30;k++){
				
				NSString *item=[temp objectAtIndex:count1-1];	
				if([item length]==2)
					printf ("%s ",[item cString]);	
				if([item length]==1)
					printf ("%s  ", [item cString]);
				count1++;
			}
			if(count1<=30){
				NSString *item=[temp objectAtIndex:count1-1];
				if([item length]==2)
					printf ("%s ",[item cString]);	
				if([item length]==1)
					printf ("%s  ", [item cString]);
			}			
			count1++;
			if(j==1)
				printf ("  [BOTON %d]",count);
			printf ("\t ");
		}
		printf ("\n ");
	}
	
	}
	
	

	
	
NSMutableArray* clientes(NSMutableArray *temp1){ //Metodo que crea los usuarios (nombre, apellido) con sus contrase�as numericas establecidas y alfabeticas generadas al azar
	int count=0;
		Tarjeta *t1 = [[Tarjeta alloc] init];
		Tarjeta *t2 = [[Tarjeta alloc] init];
		Tarjeta *t3 = [[Tarjeta alloc] init];
		Tarjeta *t4 = [[Tarjeta alloc] init];
		Tarjeta *t5 = [[Tarjeta alloc] init];
		Tarjeta *t6 = [[Tarjeta alloc] init];
		//GENERAR ID
		[t1 AumentarID:0 ];
		[t2 AumentarID:1 ];
		[t3 AumentarID:2 ];
		[t4 AumentarID:3 ];
		[t5 AumentarID:4 ];
		[t6 AumentarID:5 ];
		
		[t1 setMonto:2000];
		[t2 setMonto:1000];
		[t3 setMonto:200];
		[t4 setMonto:240];
		[t5 setMonto:302];
		[t6 setMonto:40];
		
		
		
		NSMutableArray *tarjetas = [[NSMutableArray alloc] init];
		[tarjetas addObject:t1];
		[tarjetas addObject:t2];
		[tarjetas addObject:t3];
		[tarjetas addObject:t4];
		[tarjetas addObject:t5];
		[tarjetas addObject:t6];

		//GENERAR CLAVES-Guardandolo en un archivo; si no existe o leyendolo de un archivo si ya existe.
		NSMutableArray *claves = [[NSMutableArray alloc] init];
		NSFileHandle * archivo;
		archivo = [NSFileHandle fileHandleForReadingAtPath: @"Contrasenas"];
		if (archivo == nil){	//Si no exite el archivo genero las claves y las guardo en un archivo	
			NSMutableArray *temp= [[NSMutableArray alloc] init];
			generarClaveTarjeta(temp1,temp, t1);
			generarClaveTarjeta(temp1,temp, t2);
			generarClaveTarjeta(temp1,temp, t3);
			generarClaveTarjeta(temp1,temp, t4);
			generarClaveTarjeta(temp1,temp, t5);
			generarClaveTarjeta(temp1,temp, t6);
			
			[claves addObject:[t1 getContrasena]];
			[claves addObject:[t2 getContrasena]];
			[claves addObject:[t3 getContrasena]];
			[claves addObject:[t4 getContrasena]];
			[claves addObject:[t5 getContrasena]];
			[claves addObject:[t6 getContrasena]];
			//Guardo las contrase�as generadas aleatoriamente a los clientes
			[claves writeToFile:@"Contrasenas" atomically:YES];
		}
		else{
			NSMutableArray *clavesFromFile = [[NSMutableArray alloc] initWithContentsOfFile:@"Contrasenas"];
			for(NSMutableArray *item in clavesFromFile){
				Tarjeta* t= [tarjetas objectAtIndex:count];
				[t setContrasena:item];
				count++;
			}
		}
		
		//Crea los objetos personas
		Persona * c1 = [[Persona alloc] init];
		Persona * c2 = [[Persona alloc] init];
		Persona * c3 = [[Persona alloc] init];
		Persona * c4 = [[Persona alloc] init];
		Persona * c5 = [[Persona alloc] init];
		Persona * c6 = [[Persona alloc] init];
		[c1 setPersona:@"Maria Alejandra":@"Carrion Velez":1234];
		[c2 setPersona:@"Karla Estefania":@"Perez Valdez":2356];
		[c3 setPersona:@"Tomas Ian":@"Moreira Jaramillo":4567];
		[c4 setPersona:@"Elen Carolina":@"Andrade Naranjo":2356];
		[c5 setPersona:@"Melissa Daniela":@"Cedeno Leon":2327];
		[c6 setPersona:@"Arturo Daniel":@"Arias Martino":6799];
		[c1 setTarjeta: t1];
		[c2 setTarjeta: t2];
		[c3 setTarjeta: t3];
		[c4 setTarjeta: t4];
		[c5 setTarjeta: t5];
		[c6 setTarjeta: t6];
		NSMutableArray *list= [[NSMutableArray alloc] init];
		
		//Guardarlo en una lista
		[list addObject:c1];
		[list addObject:c2];
		[list addObject:c3];
		[list addObject:c4];
		[list addObject:c5];
		[list addObject:c6];
		
		return list;
}


NSString* nuevaClaveTarjeta(NSString *ID, NSMutableArray *list){ //genera una nueva clave si se desea modificar la combinacion alfabetica establecida
	NSMutableArray *temp= [[NSMutableArray alloc] init];
	NSMutableArray *temp1= [[NSMutableArray alloc] init];
	NSMutableArray *claves= [[NSMutableArray alloc] init];
	NSMutableArray *tClave;
	for(Persona *p in list){
		Tarjeta *t = [p getTarjeta];
		[temp addObject:convertirArray_String([t getContrasena]) ];		
	}
	for(Persona *p in list){
		Tarjeta *t = [p getTarjeta];
		if([[t getID] isEqualToString:ID]==1){
			generarClaveTarjeta(temp1,temp, t);
			tClave = [t getContrasena];
		}
	}
	for(Persona *p in list){
		Tarjeta *t = [p getTarjeta];
		[claves addObject:[t getContrasena]];		
	}
	//NSFileManager *gestorArchivos = [NSFileManager defaultManager];
	//NSString *ruta = [NSTemporaryDirectory() stringByAppendingPathComponent:@"Contrasenas.txt"];
	//[gestorArchivos removeItemAtPath: ruta error: NULL];
	[claves writeToFile:@"Contrasenas" atomically:YES];
	return convertirClave_String(tClave);
}


void generarClaveTarjeta(NSMutableArray *temp,NSMutableArray *temp1, Tarjeta *t){ //Genera la clave de la tarjeta aleatoriamenete 
	int rep=-1;
	NSMutableArray *temp2;
	temp2=temp;
	NSMutableArray *clave= [[NSMutableArray alloc] init];
	NSString *key;
		
	clave = Tentativa_Claves(clave,temp2,4,3,1);
	key= convertirClave_String(clave);
	rep= repetido(key, temp1);
	printf("%d", rep);
	//temp2=temp;
	while(rep!=0){ //Verifica que no este repetida con los demas usuarios
		clave= [[NSMutableArray alloc] init];
		clave = Tentativa_Claves(clave,temp2,4,3,1);
		key= convertirArray_String(clave);
		rep= repetido(key, temp1);
	}
	
	[t setContrasena:clave];
	[temp1 addObject: key];
	
	//[clave removeAllObjects];
	//[clave release];
	
	//[temp removeAllObjects];
	//[temp release];
}




NSString* convertirArray_String(NSMutableArray *clave){ //Convierte un array en string , para facilitar la impresion, y manejo de datos
	int flag=0;
	NSString *key;
	
	for(NSString *item in clave){
		if(flag==0)
			key=item;
		else
			key= [key stringByAppendingString:item];
		flag++;
		/*key=item;
		if([item length]==2){
			key= [key stringByAppendingString:item];
			//key= [key stringByAppendingString:@" "];
		}
		if([item length]==1){
			key= [key stringByAppendingString:item];
			//key= [key stringByAppendingString:@"  "];
		}*/
		
		
	}
	//printf("%s",[key cString]);
	return key;
}

NSString* convertirClave_String(NSMutableArray *clave){ //Convierte la clave con formato establecido, en string , para facilitar la impresion, y manejo de datos
	int flag=0;
	NSString *key,*key1=@" ",*key2=@"  ";
	
	for(NSString *item in clave){
		if(flag==0){
			if([item length]==2){				
				key=item;
				key= [key stringByAppendingString:key1];
				
			}
			if([item length]==1){
				key=item;
				key= [key stringByAppendingString:key2];
			}
		}
		else{
			if([item length]==2){				
				key= [key stringByAppendingString:item];
				key= [key stringByAppendingString:key1];
				
			}
			if([item length]==1){
				key= [key stringByAppendingString:item];
				key= [key stringByAppendingString:key2];
			}
		}
		flag++;
	}
	//printf("%s",[key cString]);
	return key;
}

void mostrar_Inicio(){ //DISE�O INICIO
	
	printf("\n\t\t\txxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx");
	printf("\n\t\t\tx		  _____ ___      __ ____ ___   ____ 			x");
	printf("\n\t\t\tx		 / ___// _ | __ / // __// _ %c / __ %c			x",92,92);		
	printf("\n\t\t\tx		/ /__ / __ |/ // // _/ / , _// /_/ /			x");
	printf("\n\t\t\tx		%c___//_/ |_|%c___//___//_/|_| %c____/			x",92,92,92);
	printf("\n\t\t\tx     ------------------------------------------------------------	x");
	printf("\n\t\t\tx	 ___  ___  ___  _  _ __   __ ___  _  _  ___  ___    ___  	x");
	printf("\n\t\t\tx	| _ )|_ _|| __|| %c| |%c %c / /| __|| %c| ||_ _||   %c  / _ %c 	x",92,92,92,92,92,92);
	printf("\n\t\t\tx	| _ %c | | | _| | .` | %c V / | _| | .` | | | | |) || (_) |	x",92,92);
	printf("\n\t\t\tx	|___/|___||___||_|%c_|  %c_/  |___||_|%c_||___||___/  %c___/	x",92,92,92,92);
	printf("\n\t\t\tx									x");
	printf("\n\t\t\tx		          ��������������				x");
	printf("\n\t\t\tx		        ���______________���� 				x");
	printf("\n\txxxxxxxxxxxxx   x		     ��� __________________���� 			x xxxxxxxxxxxxx");
	printf("\n\tx  BOTON 1  x   x		   �� ______��______��________��			x x  BOTON 2  x");
	printf("\n\txxxxxxxxxxxxx	x		  � _________��______��___���___��			x xxxxxxxxxxxxx");
	printf("\n\t\t\tx		 ��____��____��______��_____��__ ��			x");
	printf("\n\txxxxxxxxxxxxx	x		 �� __���_____��______��____����_ �� 			x xxxxxxxxxxxxx");
	printf("\n\tx  BOTON 3  x   x		 �� __�����_________________��_��_�� 			x x  BOTON 4  x");
	printf("\n\txxxxxxxxxxxxx\tx		 � __��__��________________��____�� 			x xxxxxxxxxxxxx");
	printf("\n\t\t\tx		  �� _____��____________���_____�� 			x");
	printf("\n\txxxxxxxxxxxxx	x		   �� ______����_____����______��			x xxxxxxxxxxxxx");
	printf("\n\tx  BOTON 5  x   x		    �� ________�������________��	CONTINUAR->	x x  BOTON 6  x");
	printf("\n\txxxxxxxxxxxxx	x		      ��� ________________��� 				x xxxxxxxxxxxxx");
	printf("\n\t\t\tx		          ����������������				x");
	printf("\n\t\t\txxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx");
	
	//NSString *string = [NSString stringWithFormat:@"%c", asciiCode]; // A
}

void pedirID_part3(){ //DISE�O pedirID_Tarjeta
	printf("\n\txxxxxxxxxxxxx   x			xxxxxxxxxxxxxxxxxxxxxxxxx			x  xxxxxxxxxxxxx");
	printf("\n\tx  BOTON 3  x   x									x  x  BOTON 4  x");
	printf("\n\txxxxxxxxxxxxx   x									x  xxxxxxxxxxxxx");
	printf("\n\t\t\tx									x");
	printf("\n\txxxxxxxxxxxxx   x									x  xxxxxxxxxxxxx");				
	printf("\n\tx  BOTON 5  x   x	<-CANCELAR				        CONTINUAR->     x  x  BOTON 6  x");
	printf("\n\txxxxxxxxxxxxx   x									x  xxxxxxxxxxxxx");
	printf("\n\t\t\tx									x");
	printf("\n\t\t\txxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx");
}

void pedirID_part1(NSString *ID, NSString *correcta){//DISE�O pedirID_Tarjeta
	printf("\n\t\t\txxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx");
	printf("\n\t\t\tx		  _____ ___      __ ____ ___   ____ 			x");
	printf("\n\t\t\tx		 / ___// _ | __ / // __// _ %c / __ %c			x",92,92);		
	printf("\n\t\t\tx		/ /__ / __ |/ // // _/ / , _// /_/ /			x");
	printf("\n\t\t\tx		%c___//_/ |_|%c___//___//_/|_| %c____/			x",92,92,92);
	printf("\n\t\t\tx     ------------------------------------------------------------	x");
	printf("\n\t\t\tx									x");	
	printf("\n\t\t\tx	___________________________					x");
	printf("\n\t\t\tx	 INGRESE ID DE LA TARJETA:					x");
	printf("\n\txxxxxxxxxxxxx   x	---------------------------					x  xxxxxxxxxxxxx");
	printf("\n\tx  BOTON 1  x   x									x  x  BOTON 2  x");
	printf("\n\txxxxxxxxxxxxx   x			xxxxxxxxxxxxxxxxxxxxxxxxx			x  xxxxxxxxxxxxx");
	printf("\n\t\t\tx			X	%s		X			x",[ID cString]);
	printf("\n\txxxxxxxxxxxxx   x			xxxxxxxxxxxxxxxxxxxxxxxxx			x  xxxxxxxxxxxxx");
	printf("\n\tx  BOTON 3  x   x									x  x  BOTON 4  x");
	printf("\n\txxxxxxxxxxxxx   x									x  xxxxxxxxxxxxx");
	printf("\n\t\t\tx	%s						x",[correcta cString]);
	printf("\n\txxxxxxxxxxxxx   x									x  xxxxxxxxxxxxx");				
	printf("\n\tx  BOTON 5  x   x	<-CANCELAR				        CONTINUAR->     x  x  BOTON 6  x");
	printf("\n\txxxxxxxxxxxxx   x									x  xxxxxxxxxxxxx");
	printf("\n\t\t\tx									x");
	printf("\n\t\t\txxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx");
}

void pedirID_part2(){//DISE�O pedirID_Tarjeta

	printf("\n\t\t\tx			X			X			x");
}

void Encabezado(){ //Dise�o ENCABEzADO
	printf("\n\t\t\txxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx");
	printf("\n\t\t\tx		  _____ ___      __ ____ ___   ____ 			x");
	printf("\n\t\t\tx		 / ___// _ | __ / // __// _ %c / __ %c			x",92,92);		
	printf("\n\t\t\tx		/ /__ / __ |/ // // _/ / , _// /_/ /			x");
	printf("\n\t\t\tx		%c___//_/ |_|%c___//___//_/|_| %c____/			x",92,92,92);
	printf("\n\t\t\tx     ------------------------------------------------------------	x");
}


NSString *printMascaraPartes(NSMutableArray *temp, int a, int b){ //toma una parte de un Array y lo devuelve como string, Util para imprimir las 6 m�scaras
	int i;
	NSMutableArray *cadena= [[NSMutableArray alloc] init];
	NSString *result;
	for (i=a; i<= b;i++){		
		NSString *item=[temp objectAtIndex:i];
		[cadena addObject: item];		
	}
	result=convertirClave_String(cadena);
	return result;
}

void pedirContrasena(NSMutableArray *temp){ //DISE�O de pedir contrase�a
	Encabezado();
				printf("\n\t\t\tx	_____________________________________				x");
				printf("\n\t\t\tx	 Por favor ingrese su clave personal:				x");
				printf("\n\t\t\tx	-------------------------------------				x");
	printf("\n\txxxxxxxxxxxxx	x	xxxxxxxxxxxxxxxxxxxx		  xxxxxxxxxxxxxxxxxxxx		x	xxxxxxxxxxxxx");
	printf("\n\tx  BOTON 1  x	x	*  %s *		  *  %s *		x	x  BOTON 2  x",[printMascaraPartes(temp,0,4) cString],[printMascaraPartes(temp,5,9) cString]);
	printf("\n\txxxxxxxxxxxxx	x	xxxxxxxxxxxxxxxxxxxx		  xxxxxxxxxxxxxxxxxxxx		x	xxxxxxxxxxxxx");
				printf("\n\t\t\tx									x");
	printf("\n\txxxxxxxxxxxxx	x	xxxxxxxxxxxxxxxxxxxx		  xxxxxxxxxxxxxxxxxxxx		x	xxxxxxxxxxxxx");
	printf("\n\tx  BOTON 3  x	x	*  %s *		  *  %s *		x	x  BOTON 4  x",[printMascaraPartes(temp,10,14) cString],[printMascaraPartes(temp,15,19) cString]);
	printf("\n\txxxxxxxxxxxxx	x	xxxxxxxxxxxxxxxxxxxx		  xxxxxxxxxxxxxxxxxxxx		x	xxxxxxxxxxxxx");
				printf("\n\t\t\tx									x");
	printf("\n\txxxxxxxxxxxxx	x	xxxxxxxxxxxxxxxxxxxx		  xxxxxxxxxxxxxxxxxxxx		x	xxxxxxxxxxxxx");			
	printf("\n\tx  BOTON 5  x	x	*  %s *		  *  %s *		x	x  BOTON 6  x",[printMascaraPartes(temp,20,24) cString],[printMascaraPartes(temp,25,29) cString]);
	printf("\n\txxxxxxxxxxxxx	x	xxxxxxxxxxxxxxxxxxxx		  xxxxxxxxxxxxxxxxxxxx		x	xxxxxxxxxxxxx");
				printf("\n\t\t\tx									x");
				printf("\n\t\t\txxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx");

}

void retiros_inicio(){ //DISE�O retiros
	Encabezado();
				printf("\n\t\t\tx	______________________________________				x");
				printf("\n\t\t\tx	 Por favor, escoja una de las opciones:				x");
				printf("\n\t\t\tx	--------------------------------------				x");
	printf("\n\txxxxxxxxxxxxx	x									x  xxxxxxxxxxxxx");
	printf("\n\tx  BOTON 1  x	x									x  x  BOTON 2  x");
	printf("\n\txxxxxxxxxxxxx	x									x  xxxxxxxxxxxxx");
				printf("\n\t\t\tx									x");
	printf("\n\txxxxxxxxxxxxx	x	_____________________		_____________________		x  xxxxxxxxxxxxx");
	printf("\n\tx  BOTON 3  x	x	 <--	RETIROS			CAMBIO DE MASCARA -->		x  x  BOTON 4  x");
	printf("\n\txxxxxxxxxxxxx	x	---------------------		---------------------		x  xxxxxxxxxxxxx");
				printf("\n\t\t\tx									x");
	printf("\n\txxxxxxxxxxxxx   x									x  xxxxxxxxxxxxx");				
	printf("\n\tx  BOTON 5  x   x	<-CANCELAR				        CONTINUAR->     x  x  BOTON 6  x");
	printf("\n\txxxxxxxxxxxxx   x									x  xxxxxxxxxxxxx");
				printf("\n\t\t\tx									x");
				printf("\n\t\t\txxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx");

}

void retiros_pedirValor(NSString *valor, NSString *correcta){ //Dise�o retiros-con valor
	Encabezado();
	printf("\n\t\t\tx									x");	
				printf("\n\t\t\tx	_______________________________________				x");
				printf("\n\t\t\tx	 Por favor, Ingrese la cantidad deseada:			x");
	printf("\n\txxxxxxxxxxxxx   x	---------------------------------------				x  xxxxxxxxxxxxx");
	printf("\n\tx  BOTON 1  x   x									x  x  BOTON 2  x");
	printf("\n\txxxxxxxxxxxxx   x			xxxxxxxxxxxxxxxxxxxxxxxxx			x  xxxxxxxxxxxxx");
	printf("\n\t\t\tx			X	%s		X			x",[valor cString]);
	printf("\n\txxxxxxxxxxxxx   x			xxxxxxxxxxxxxxxxxxxxxxxxx			x  xxxxxxxxxxxxx");
	printf("\n\tx  BOTON 3  x   x									x  x  BOTON 4  x");
	printf("\n\txxxxxxxxxxxxx   x									x  xxxxxxxxxxxxx");
	printf("\n\t\t\tx	%s						x",[correcta cString]);
	printf("\n\txxxxxxxxxxxxx   x									x  xxxxxxxxxxxxx");				
	printf("\n\tx  BOTON 5  x   x	          				        ACEPTAR->       x  x  BOTON 6  x");
	printf("\n\txxxxxxxxxxxxx   x									x  xxxxxxxxxxxxx");
	printf("\n\t\t\tx									x");
	printf("\n\t\t\txxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx");
}

void cambiarMascara(NSString*antigua, NSString *nueva){ //DISE�O opcion Cambiar mascara
	Encabezado();
				printf("\n\t\t\tx	__________________						x");
				printf("\n\t\t\tx	 CAMBIO DE MASCARA:						x");
				printf("\n\t\t\tx	------------------						x");
	printf("\n\txxxxxxxxxxxxx   x									x  xxxxxxxxxxxxx");
	printf("\n\tx  BOTON 1  x   x	----->> ANTIGUA MASCARA:   %s				x  x  BOTON 2  x",[antigua cString]);
	printf("\n\txxxxxxxxxxxxx   x				---------------				x  xxxxxxxxxxxxx");
				printf("\n\t\t\tx									x");
	printf("\n\txxxxxxxxxxxxx   x									x  xxxxxxxxxxxxx");
	printf("\n\tx  BOTON 3  x   x	----->> NUEVA MASCARA:	%s				x  x  BOTON 4  x",[nueva cString]);
	printf("\n\txxxxxxxxxxxxx   x				---------------				x  xxxxxxxxxxxxx");
				printf("\n\t\t\tx									x");
	printf("\n\txxxxxxxxxxxxx   x									x  xxxxxxxxxxxxx");				
	printf("\n\tx  BOTON 5  x   x	          				        ACEPTAR->       x  x  BOTON 6  x");
	printf("\n\txxxxxxxxxxxxx   x									x  xxxxxxxxxxxxx");
				printf("\n\t\t\tx									x");
				printf("\n\t\t\txxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx");


}