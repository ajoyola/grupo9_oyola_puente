
;; ******  DECLARACI�N DE VARIABLES, FUNCIONES Y PROCEDIMENTOS

(defvar dato) ; variable que contendr� el user y correo ingresado por el usuario

;----------------- BASE DE DATOS-----------------------

;______________MATERIAS_________________
(defun make_listado_materias (codigo nombre horas p_tareas p_examenes p_proyectos)
	(list :codigo codigo :nombre nombre :horas horas
	      :p_tareas p_tareas :p_examenes p_examenes :p_proyectos p_proyectos))
        ;(list codigo nombre horas))

(defvar *Db_Materias* nil)
(defvar Materias nil)

(defun add_materia (listado_materia) (push listado_materia *Db_Materias*))

(defun pedir_read (prompt)
  (format *query-io* "~a: " prompt)
  (force-output *query-io*) ;Force-output es necesario cuando se quiere esperar un nueva linea
  (read-line *query-io*))
 
; PEDIR DATOS
(defun  pedir_materias ()
	(make_listado_materias
		(pedir_read "Codigo")
		(pedir_read "Nombre")
		(or (parse-integer (pedir_read "Horas") :junk-allowed t) 0)
		(or (parse-integer (pedir_read "%Tareas") :junk-allowed t) 0)
		(or (parse-integer (pedir_read "%Examenes") :junk-allowed t) 0)
		(or (parse-integer (pedir_read "%Proyectos") :junk-allowed t) 0)))
 
 (defun add_listado_materias ()
	(loop (add_materia (pedir_materias))
		(if (not (y-or-n-p "Desea a�adir otro? [y/n]: ")) (return))))

;Guardar base de datos materias
(defun save_Db_materias (filename)
  (setf Materias *Db_Materias*)
  (with-open-file (out filename
		   :direction :output
		   :if-exists :supersede)
    (with-standard-io-syntax
      (print *Db_Materias* out))))
     ; (format out (first Materias)))  ;;Intentar guardar con formato

;Cargar base de datos materias
(defun load_Db_Materias (filename)
  (with-open-file (in filename)
    (with-standard-io-syntax
      (setf *DB_Materias* (read in)))))

;Select materia //ALTERNATIVA
(defun get_codigo_materia (codigo)
  #'(lambda (listado_materias) (equal (getf listado_materias :codigo) codigo)))

(defun select_materia1(codigo)
  (remove-if-not (get_codigo_materia codigo) *Db_Materias*))
 

;WHERE-SELECT materia
(defun where_materia (&key codigo nombre horas p_tareas p_examenes p_proyectos)
  #'(lambda (materias)
      (and
       (if codigo (equal (getf materias :codigo) codigo) t)
       (if nombre (equal (getf materias :nombre) nombre) t)
       (if horas (equal (getf materias :horas) horas) t)
       (if p_tareas (equal (getf materias :p_tareas) p_tareas) t) 
       (if p_examenes (equal (getf materias :p_examenes) p_examenes) t)
       (if p_proyectos (equal (getf materias :p_proyectos) p_proyectos) t))))

(defun select_materia_codigo(codigo)
  (remove-if-not (where_materia :codigo codigo) *Db_Materias*))

(defun select_materia_nombre(nombre)
  (remove-if-not (where_materia :nombre nombre) *Db_Materias*))

(defun select_materia_horas(horas)
  (remove-if-not (where_materia :horas horas) *Db_Materias*))

(defun select_materia_p_tareas(p_tareas)
  (remove-if-not (where_materia :p_tareas p_tareas) *Db_Materias*))

(defun select_materia_p_examenes(p_examenes)
  (remove-if-not (where_materia :p_examenes p_examenes) *Db_Materias*))

(defun select_materia_p_proyectos(p_proyectos)
  (remove-if-not (where_materia :p_proyectos p_proyectos) *Db_Materias*))

(defun select_materia(selector_fn)
  (remove-if-not selector_fn *Db_Materias*))

;GETTERS- SETTERS
(defun get_nombre_materia(selector_fn)
  (setq Materias (select_materia selector_fn))
  (print (nth 3 (first Materias))))

;UPDATE materia
(defun update_materia (selector_fn &key codigo nombre horas p_tareas p_examenes p_proyectos)
  (setf *Db_Materias*
	(mapcar
	 #'(lambda (row)
	     (when (funcall selector_fn row)
	        (if codigo (setf (getf row :codigo) codigo))
			(if nombre (setf (getf row :nombre) nombre))
			(if horas (setf (getf row :horas) horas))
			(if p_tareas (setf (getf row :p_tareas) p_tareas))
			(if p_examenes (setf (getf row :p_examenes) p_examenes ))
			(if p_proyectos (setf (getf row :p_proyectos) p_proyectos)))
			 row) *Db_Materias*))
  (save_Db_materias "Materias.db")) ;Despues de la modficacion guardar cambios

;REMOVE materia
(defun remove_materia (selector_fn )
  (setf *Db_Materias* (remove-if selector_fn *Db_Materias*))
  (save_Db_materias "Materias.db"));Guardar cambios

;Sublistas
 (defun sublistas (expresion)
   (cond ((or (null expresion) (atom expresion)) 0)
	 (t (+ (if (atom (first expresion)) 0 1)
	       (sublistas (first expresion))
	       (sublistas (rest expresion))))))

;SUMA: Esta funci�n va a sumar un contenido (de valor numerico) de una lista
(defun sumar_val_lista (lista pos)
	(cond ((or (null lista) (atom lista)) 0)
		(t	(+ (nth pos (first lista))
			   (sumar_val_lista (rest lista) pos)))))
			   
;VALIDAR DUPLICADOS
(defun comparar_exp1 (field value)
	(list 'equal (list 'getf 'listado field) value))

(defun comparar_exp (field value)
	`(equal (getf listado ,field) ,value))

(defun comparar_listas(fields)
	(loop while fields
		collecting (comparar_exp (pop fields) (pop fields))))

(defmacro where_dupl (&rest clauses)
	`#'(lambda (listado) (and ,@(comparar_listas clauses))))		
		   
		   
;Presentar Datos

(defun print_materia (lista1 lista2)
 (setq lista2 lista1) ;pasar datos de una variable global a variable local para poder acceder a los metodos de una lista.

  (cond
    ((null lista2) NIL)
    ((atom (first lista2))
     (format t "~%~A~20T~A~50T~A" 

	     (nth 1 (first lista2)) 
	     (nth 3 (first lista2))
	     (nth 5 (first lista2)))
     (print_materia (rest lista2) (rest lista2)))
    (t 
     (format t "~%~A~20T~A~50T~A" 

	     (nth 1 (first lista2)) 
	     (nth 3 (first lista2))
	     (nth 5 (first lista2)))
     (print_materia (rest lista2) (rest lista2)))))

(defun print_listado_materia()
  (format t "~% LISTADO:~%")
  (format t "Codigo~20TNombre~50THoras~%")
  (setq Materias *Db_Materias*)
  (print_materia *Db_Materias* Materias)

)



;______________ASISTENCIAS_________________
(defun make_listado_asistencia (IDMateria Num_Clase isAsistio)
	(list :IDMateria IDMateria :Num_Clase Num_Clase :isAsistio isAsistio))

(defvar *Db_Asistencias* nil)
(defvar Asistencias nil)

(defun add_asistencia (listado_asistencia) (push listado_asistencia *Db_Asistencias*))

 
; PEDIR DATOS
(defun  pedir_asistencia ()
	(make_listado_asistencia
		(pedir_read "IDMateria")
		(or (parse-integer (pedir_read "Num_Clase") :junk-allowed t) 0)
		(y-or-n-p "isAsistio [y/n]: ")))
 
 (defun add_listado_asistencia ()
	(loop (add_asistencia (pedir_asistencia))
		(if (not (y-or-n-p "Desea a�adir otro? [y/n]: ")) (return)))
	(save_db_asistencia "Asistencia.db"))

;Pedir datos con IDMateria 
(defun pedir_asistencia_idMat (idMateria)
  (make_listado_asistencia
   idMateria
   (or (parse-integer (pedir_read "Num_Clase") :junk-allowed t) 0)
   (y-or-n-p "isAsistio [y/n]: ")))

(defun add_listado_asistencia_idMat (idMateria)
  (loop (add_asistencia (pedir_asistencia_idMat idMateria))
     (if (not (y-or-n-p "Desea a�adir otro? [y/n]: ")) (return)))
  (save_db_asistencia "Asistencia.db"))


;Dado una materia especifica, con un listado de dias de clases (no)asistidas lo guarda en conjunto en la base

(defun add_asistencia_dias(idMateria lista isAsistio)
  (cond
    ((null lista) NIL)
    ((atom (first lista))
     (add_asistencia (make_listado_asistencia 
      idMateria
      (first lista)
      isAsistio))
     (add_asistencia_dias idMateria (rest lista) isAsistio))
    (t 
     (add_asistencia (make_listado_asistencia 
      idMateria
      (first lista)
      isAsistio))
      (add_asistencia_dias idMateria (rest lista) isAsistio)))
  (save_db_asistencia "Asistencia.db"))

;Guardar base de datos asistencia
(defun save_Db_asistencia (filename)
  (setf Asistencias *Db_Asistencias*)
  (with-open-file (out filename
		   :direction :output
		   :if-exists :supersede)
    (with-standard-io-syntax
      (print *Db_Asistencias* out))))
     ; (format out (first Asistencias)))  ;;Intentar guardar con formato

;Cargar base de datos asistencia
(defun load_Db_Asistencias (filename)
  (with-open-file (in filename)
    (with-standard-io-syntax
      (setf *DB_Asistencias* (read in)))))


;WHERE-SELECT asistencia
(defun where_asistencia (&key idMateria num_clase isAsistio)
  #'(lambda (materias)
      (and
       (if idMateria (equal (getf materias :IDMateria) idMateria) t)
       (if num_clase (equal (getf materias :Num_Clase) num_clase) t)
       (if isAsistio (equal (getf materias :isAsistio) isAsistio) t))))

(defun select_asistencia_iDMateria(IDMateria)
  (remove-if-not (where_asistencia :IDMateria IDMateria) *Db_Asistencias*))

(defun select_asistencia_num_Clase(Num_Clase)
  (remove-if-not (where_asistencia :Num_Clase Num_Clase) *Db_Asistencias*))

(defun select_asistencia_isAsistio(isAsistio)
  (remove-if-not (where_asistencia :isAsistio isAsistio) *Db_Asistencias*))

(defun select_asistencia(selector_fn)
  (remove-if-not selector_fn *Db_Asistencias*))

;UPDATE materia
(defun update_asistencia (selector_fn &key idMateria num_clase isAsistio)
  (setf *Db_Asistencias*
	(mapcar
	 #'(lambda (row)
	     (when (funcall selector_fn row)
	        (if idMateria (setf (getf row :IDMateria) idMateria))
		(if num_clase (setf (getf row :Num_clase) num_clase))
		(if isAsistio (setf (getf row :isAsistio) isAsistio)))
	     row) *Db_Asistencias*))
  (save_Db_asistencia "Asistencias.db")) ;Despues de la modficacion guardar cambios

;REMOVE materia
(defun remove_asistencia (selector_fn )
  (setf *Db_Asistencias* (remove-if selector_fn *Db_Asistencias*))
  (save_Db_asistencia "Asistencias.db"));Guardar cambios

;Presentar Datos
(defun print_asistencia (lista1 lista2)
  (setq lista2 lista1)
  (cond
    ((null lista2) NIL)
    ((atom (first lista2))
     (format t "~%Clase #~A~20T~A" 
	    ; (nth 1 (first lista2)) 
	     (nth 3 (first lista2))
	     (nth 5 (first lista2)))
     (print_asistencia (rest lista2) (rest lista2)))
    (t 
     (format t "~%Clase #~A~20T~A" 
	     ;(nth 1 (first lista2)) 
	     (nth 3 (first lista2))
	     (nth 5 (first lista2)))
     (print_asistencia (rest lista2) (rest lista2)))))





;___________________________TAREAS_______________________________
(defun make_listado_tarea (IDMateria Num_Tarea isCumplio)
	(list :IDMateria IDMateria :Num_Tarea Num_Tarea :isCumplio isCumplio))

(defvar *Db_Tareas* nil)
(defvar Tareas nil)

(defun add_tarea (listado_tarea) (push listado_tarea *Db_Tareas*))

 
; PEDIR DATOS
(defun  pedir_tarea ()
	(make_listado_tarea
		(pedir_read "IDMateria")
		(or (parse-integer (pedir_read "Num_Tarea") :junk-allowed t) 0)
		(or (parse-integer (pedir_read "isCumplio") :junk-allowed t) 0)))
 
 (defun add_listado_tarea ()
	(loop (add_tarea (pedir_tarea))
		(if (not (y-or-n-p "Desea a�adir otro? [y/n]: ")) (return)))
	(save_db_tarea "Tareas.db"))

;Pedir datos con IDMateria 
(defun pedir_tarea_idMat (idMateria)
  (make_listado_tarea
   idMateria
   (or (parse-integer (pedir_read "Num_Tarea") :junk-allowed t) 0)
   (or (parse-integer (pedir_read "isCumplio") :junk-allowed t) 0)))

(defun add_listado_tarea_idMat (idMateria)
  (loop (add_tarea (pedir_tarea_idMat idMateria))
     (if (not (y-or-n-p "Desea a�adir otro? [y/n]: ")) (return)))
  (save_db_asistencia "Tareas.db"))


;Guardar base de datos tarea
(defun save_Db_tarea (filename)
  (setf Tareas *Db_Tareas*)
  (with-open-file (out filename
		   :direction :output
		   :if-exists :supersede)
    (with-standard-io-syntax
      (print *Db_Tareas* out))))
     ; (format out (first Asistencias)))  ;;Intentar guardar con formato

;Cargar base de datos tarea
(defun load_Db_Tareas (filename)
  (with-open-file (in filename)
    (with-standard-io-syntax
      (setf *Db_Tareas* (read in)))))


;WHERE-SELECT tarea
(defun where_tarea (&key idMateria num_tarea isCumplio)
  #'(lambda (lista)
      (and
       (if idMateria (equal (getf lista :IDMateria) idMateria) t)
       (if num_tarea (equal (getf lista :Num_Tarea) num_tarea) t)
       (if isCumplio (equal (getf lista :isCumplio) isCumplio) t))))

(defun select_tarea_iDMateria(IDMateria)
  (remove-if-not (where_tarea :IDMateria IDMateria) *Db_Tareas*))

(defun select_tarea_num_tarea(Num_Tarea)
  (remove-if-not (where_tarea :Num_Tarea Num_Tarea) *Db_Tareas*))

(defun select_tarea_isCumplio (isCumplio)
  (remove-if-not (where_tarea :isCumplio isCumplio) *Db_Tareas*))

(defun select_tarea(selector_fn)
  (remove-if-not selector_fn *Db_Tareas*))

;UPDATE tarea
(defun update_tarea (selector_fn &key idMateria num_tarea isCumplio)
  (setf *Db_Tareas*
	(mapcar
	 #'(lambda (row)
	     (when (funcall selector_fn row)
	        (if idMateria (setf (getf row :IDMateria) idMateria))
		(if num_tarea (setf (getf row :Num_tarea) num_tarea))
		(if isCumplio (setf (getf row :isCumplio) isCumplio)))
	     row) *Db_Tareas*))
  (save_Db_tarea "Tareas.db")) ;Despues de la modficacion guardar cambios

;REMOVE tarea
(defun remove_tarea (selector_fn )
  (setf *Db_Tareas* (remove-if selector_fn *Db_Tareas*))
  (save_Db_tarea "Tareas.db"));Guardar cambios

;Presentar Datos
(defun print_tarea (lista1 lista2)
  (setq lista2 lista1)
  (cond
    ((null lista2) NIL)
    ((atom (first lista2))
     (format t "~%Tarea #~A~20T~A" 
	    ; (nth 1 (first lista2)) 
	     (nth 3 (first lista2))
	     (nth 5 (first lista2)))
     (print_tarea(rest lista2) (rest lista2)))
    (t 
     (format t "~%Tarea #~A~20T~A" 
	     ;(nth 1 (first lista2)) 
	     (nth 3 (first lista2))
	     (nth 5 (first lista2)))
     (print_tarea (rest lista2) (rest lista2)))))




;___________________________Examenes_______________________________
(defun make_listado_examen (IDMateria Num_examen Nota)
	(list :IDMateria IDMateria :Num_examen Num_examen :Nota Nota))

(defvar *Db_Examenes* nil)
(defvar Examenes nil)

(defun add_examen (listado_examen) (push listado_examen *Db_Examenes*))

 
; PEDIR DATOS
(defun  pedir_examen ()
	(make_listado_examen
		(pedir_read "IDMateria")
		(or (parse-integer (pedir_read "Num_examen") :junk-allowed t) 0)
		(or (parse-integer (pedir_read "Nota") :junk-allowed t) 0)))
 
 (defun add_listado_examen ()
	(loop (add_examen (pedir_examen))
		(if (not (y-or-n-p "Desea a�adir otro? [y/n]: ")) (return)))
	(save_db_examen "Examenes.db"))

;Pedir datos con IDMateria 
(defun pedir_examen_idMat (idMateria)
  (make_listado_examen
   idMateria
   (or (parse-integer (pedir_read "Num_examen") :junk-allowed t) 0)
   (or (parse-integer (pedir_read "Nota") :junk-allowed t) 0)))

(defun add_listado_examen_idMat (idMateria)
  (loop (add_examen (pedir_examen_idMat idMateria))
     (if (not (y-or-n-p "Desea a�adir otro? [y/n]: ")) (return)))
  (save_db_asistencia "Examenes.db"))


;Guardar base de datos examen
(defun save_Db_examen (filename)
  (setf Examenes *Db_Examenes*)
  (with-open-file (out filename
		   :direction :output
		   :if-exists :supersede)
    (with-standard-io-syntax
      (print *Db_Examenes* out))))
     ; (format out (first Asistencias)))  ;;Intentar guardar con formato

;Cargar base de datos examen
(defun load_Db_Examenes (filename)
  (with-open-file (in filename)
    (with-standard-io-syntax
      (setf *Db_Examenes* (read in)))))


;WHERE-SELECT examen
(defun where_examen (&key idMateria num_examen nota)
  #'(lambda (lista)
      (and
       (if idMateria (equal (getf lista :IDMateria) idMateria) t)
       (if num_examen (equal (getf lista :Num_examen) num_examen) t)
       (if nota (equal (getf lista :Nota) nota) t))))

(defun select_examen_iDMateria(IDMateria)
  (remove-if-not (where_examen :IDMateria IDMateria) *Db_Examenes*))

(defun select_examen_num_examen(Num_examen)
  (remove-if-not (where_examen :Num_examen Num_examen) *Db_Examenes*))

(defun select_examen_nota (Nota)
  (remove-if-not (where_examen :Nota Nota) *Db_Examenes*))

(defun select_examen(selector_fn)
  (remove-if-not selector_fn *Db_Examenes*))

;UPDATE examen
(defun update_examen (selector_fn &key idMateria num_examen nota)
  (setf *Db_Examenes*
	(mapcar
	 #'(lambda (row)
	     (when (funcall selector_fn row)
	        (if idMateria (setf (getf row :IDMateria) idMateria))
		(if num_examen (setf (getf row :Num_examen) num_examen))
		(if nota (setf (getf row :nota) nota)))
	     row) *Db_Examenes*))
  (save_Db_examen "Examenes.db")) ;Despues de la modficacion guardar cambios

;REMOVE examen
(defun remove_examen (selector_fn )
  (setf *Db_Examenes* (remove-if selector_fn *Db_Examenes*))
  (save_Db_examen "Examenes.db"));Guardar cambios

;Presentar Datos
(defun print_examen (lista1 lista2)
  (setq lista2 lista1)
  (cond
    ((null lista2) NIL)
    ((atom (first lista2))
     (format t "~%Examen #~A~20T~A" 
	    ; (nth 1 (first lista2)) 
	     (nth 3 (first lista2))
	     (nth 5 (first lista2)))
     (print_examen(rest lista2) (rest lista2)))
    (t 
     (format t "~%Examen #~A~20T~A" 
	     ;(nth 1 (first lista2)) 
	     (nth 3 (first lista2))
	     (nth 5 (first lista2)))
     (print_examen (rest lista2) (rest lista2)))))



;_________________Proyectos_______________________________
(defun make_listado_proyecto (IDMateria Num_proyecto Nota)
	(list :IDMateria IDMateria :Num_proyecto Num_proyecto :Nota Nota))

(defvar *Db_Proyectos* nil)
(defvar Proyectos nil)

(defun add_proyecto (listado_proyecto) (push listado_proyecto *Db_Proyectos*))

 
; PEDIR DATOS
(defun  pedir_proyecto ()
	(make_listado_proyecto
		(pedir_read "IDMateria")
		(or (parse-integer (pedir_read "Num_proyecto") :junk-allowed t) 0)
		(or (parse-integer (pedir_read "Nota") :junk-allowed t) 0)))
 
 (defun add_listado_proyecto ()
	(loop (add_proyecto (pedir_proyecto))
		(if (not (y-or-n-p "Desea a�adir otro? [y/n]: ")) (return)))
	(save_db_proyecto "Proyectos.db"))

;Pedir datos con IDMateria 
(defun pedir_proyecto_idMat (idMateria)
  (make_listado_proyecto
   idMateria
   (or (parse-integer (pedir_read "Num_proyecto") :junk-allowed t) 0)
   (or (parse-integer (pedir_read "Nota") :junk-allowed t) 0)))

(defun add_listado_proyecto_idMat (idMateria)
  (loop (add_proyecto (pedir_proyecto_idMat idMateria))
     (if (not (y-or-n-p "Desea a�adir otro? [y/n]: ")) (return)))
  (save_db_asistencia "Proyectos.db"))


;Guardar base de datos proyecto
(defun save_Db_proyecto (filename)
  (setf Proyectos *Db_Proyectos*)
  (with-open-file (out filename
		   :direction :output
		   :if-exists :supersede)
    (with-standard-io-syntax
      (print *Db_Proyectos* out))))
     ; (format out (first Asistencias)))  ;;Intentar guardar con formato

;Cargar base de datos proyecto
(defun load_Db_Proyectos (filename)
  (with-open-file (in filename)
    (with-standard-io-syntax
      (setf *Db_Proyectos* (read in)))))


;WHERE-SELECT proyecto
(defun where_proyecto (&key idMateria num_proyecto nota)
  #'(lambda (lista)
      (and
       (if idMateria (equal (getf lista :IDMateria) idMateria) t)
       (if num_proyecto (equal (getf lista :Num_proyecto) num_proyecto) t)
       (if nota (equal (getf lista :Nota) nota) t))))

(defun select_proyecto_iDMateria(IDMateria)
  (remove-if-not (where_proyecto :IDMateria IDMateria) *Db_Proyectos*))

(defun select_proyecto_num_proyecto(Num_proyecto)
  (remove-if-not (where_proyecto :Num_proyecto Num_proyecto) *Db_Proyectos*))

(defun select_proyecto_nota (Nota)
  (remove-if-not (where_proyecto :Nota Nota) *Db_Proyectos*))

(defun select_proyecto(selector_fn)
  (remove-if-not selector_fn *Db_Proyectos*))

;UPDATE proyecto
(defun update_proyecto (selector_fn &key idMateria num_proyecto nota)
  (setf *Db_Proyectos*
	(mapcar
	 #'(lambda (row)
	     (when (funcall selector_fn row)
	        (if idMateria (setf (getf row :IDMateria) idMateria))
		(if num_proyecto (setf (getf row :Num_proyecto) num_proyecto))
		(if nota (setf (getf row :nota) nota)))
	     row) *Db_Proyectos*))
  (save_Db_proyecto "Proyectos.db")) ;Despues de la modficacion guardar cambios

;REMOVE proyecto
(defun remove_proyecto (selector_fn )
  (setf *Db_Proyectos* (remove-if selector_fn *Db_Proyectos*))
  (save_Db_proyecto "Proyectos.db"));Guardar cambios

;Presentar Datos
(defun print_proyecto (lista1 lista2)
  (setq lista2 lista1)
  (cond
    ((null lista2) NIL)
    ((atom (first lista2))
     (format t "~%proyecto #~A~20T~A" 
	    ; (nth 1 (first lista2)) 
	     (nth 3 (first lista2))
	     (nth 5 (first lista2)))
     (print_proyecto(rest lista2) (rest lista2)))
    (t 
     (format t "~%proyecto #~A~20T~A" 
	     ;(nth 1 (first lista2)) 
	     (nth 3 (first lista2))
	     (nth 5 (first lista2)))
     (print_proyecto (rest lista2) (rest lista2)))))




; Desarrollo de procedimientos leer y escribir en archivo
( defun writeFile (dato)
	(with-open-file (stream "MATERIAS.txt" :direction :output :if-exists :append)
	   (format stream dato)
	   (terpri stream)
   ))
(defun readFile ()
	(let ((in (open "MATERIAS.txt" :if-does-not-exist nil)))
	  (when in
		(loop for line = (read-line in nil)
			 while line do (format t "~a~%" line))
		(close in))))


;--------------------------PROGRAMA--------------------------------

(defvar op) ; defino variable op que contendr� opcion del menu
(defvar op1) (defvar op2) ; defino variable op que contendr� opcion del menu
;(defvar Materias)

;Materias
(defvar nombre "")  (defvar codigo "") (defvar r_codigo "")(defvar horas 0)
(defvar n_mat 0) 
;Asistencias
(defvar num_clase 0) (defvar bool "T")
;Tareas
(defvar num_tarea 0) (defvar nota 0)
;Examen
(defvar num_examen 0) (defvar num_proyecto 0)

(defun programa ()
  (load_db_materias "Materias.db")
  (load_db_asistencias "Asistencias.db")
  (load_db_tareas "Tareas.db")
  (load_db_proyectos "Proyectos.db")
  (load_db_examenes "Examenes.db")
  (loop do
       ;INICIO
       (format t "~%~%                           *_*_*_BIENVENIDO_*_*_*~%")
       (format t "  MENU ~%")
       (format t "    1) Materias~%")
       (format t "    2) Asitencias~%")
       (format t "    3) Tareas~%")
       (format t "    4) Proyectos~%")
       (format t "    5) Examenes~%")
       (format t "    6) Estadisticas~%") 
       (format t "    7) Salir~%")

       (format t "~%Seleccione una opcion: ")
       (setq op (read))
       (terpri)

       (case op
	 ((1) ;MATERIAS
	  (loop do 
	       (format t "~%    -*Materias*-~%~%")
	       (format t "  1) Consultar listado~%")
	       (format t "  2) Nuevo~%")
	       (format t "  3) Editar~%")
	       (format t "  4) Eliminar~%")
	       (format t "  5) Regresar~%")
	       
	       (format t "~%Seleccione una opcion: ")
	       (setq op1 (read))
	       (terpri)

	       (case op1
		 ((1) ;Consultar Materias
		  (format t "~%    -*Listado de Materias*-~%~%")
		  (format t "Codigo~20TNombre~50THoras~%")
		  (setq Materias *Db_Materias*)
		  (print_materia *Db_Materias* Materias)
		 )

		 ((2) ;Nueva Materia
		  
		  (format t "~%   -*Nueva Materia*-~%")
		  (add_listado_materias)
		  (save_Db_materias "Materias.db")
		 )
		 ((3);Actualizacion de Materias
		  (format t "~%    -*Actualizacion de Datos Materia*-~%~%")
		  (print_listado_materia)
		  (format t "~%Seleccione materia (codigo): ")
		  (setq r_codigo (read))

		  (format t "Codigo~20TNombre~50THoras")
		 ; (select_materia_codigo r_codigo)
		  ;(setq Materias (select_materia_codigo r_codigo)) 
		  (print_materia (select_materia_codigo r_codigo)  Materias)
		
		  (if (y-or-n-p "Desea modificar CODIGO?")
		      (progn
			(format t "Ingrese codigo: ")
			(setq codigo (read))			
			(update_materia (where_materia :codigo r_codigo) :codigo codigo)
			(update_asistencia (where_asistencia :IDMateria r_codigo) :idMateria codigo)))
		  (if (y-or-n-p "Desea modificar NOMBRE? ")
		      (progn
			(format t "Ingrese nombre: ")
			(setq nombre (read))
			(update_materia (where_materia :codigo r_codigo) :nombre nombre)))
		 (if (y-or-n-p "Desea modificar HORAS? ")
		     (progn
		       (format t "Ingrese numero de horas: ")
		       (setq horas (read))
		       (update_materia (where_materia :codigo r_codigo) :horas horas)))
		  )
		 ((4) ;Eliminacion de Materias
		  (format t "~%    -*Eliminacion de Materia*-~%~%")
		  (print_listado_materia)
		  (format t "~%Seleccione materia (codigo): ")
		  (setq r_codigo (read))

		  (remove_materia (where_materia :codigo r_codigo))
		  (remove_asistencia (where_asistencia :IDMateria r_codigo))
		  (remove_tarea (where_tarea :IDMateria r_codigo))
		  (remove_proyecto (where_proyecto :IDMateria r_codigo))
		  (remove_examen (where_examen :IDMateria r_codigo))
		  (format t "~%Materia eliminada con �xito")
		  (print_listado_materia)

		  )

	       )
	   while (< op1 4))
	  (setq op 0)
	  )
	 ((2) ;ASISTENCIA
	  ;Al ingresar en esta parte del men�, se pide de antemano la materia de la cual se desea realizar acciones con la Asistencias.
	  (setq r_codigo 0)
	  (print_listado_materia)
	  (format t "~%Seleccione materia (codigo): ")
	  (setq r_codigo (read))
	  (format t "Codigo~20TNombre~50THoras")
	  (print_materia (select_materia_codigo r_codigo)  Materias)
	  
	  (loop do 
	       (format t "~%    -*Asistencias*-~%~%")
	       (format t "  1) Consultar listado~%")
	       (format t "  2) Nuevo~%")
	       (format t "  3) Editar~%")
	       (format t "  4) Eliminar~%")
	       (format t "  5) Regresar~%")
	       (format t "~%Seleccione una opcion: ")
	       (setq op1 (read))
	       (terpri)
	       
	       (case op1
		 ((1) ;Consultar Asistencias 
		  (format t "~%~%Num_Clase ~20TAsistio")
		  (setq Asistencias *Db_Asistencias*)
		  (print_asistencia (select_asistencia_idMateria r_codigo) Asistencias)		  
		 )
		 
		  ((2) ;Nueva Asistencia		  
		  (format t "~%   -*Nueva Asistencia*-~%")
		  (add_listado_asistencia_idMat r_codigo )
		  (save_Db_asistencia "Asistencias.db")
		 )
		 ((3);Actualizacion de Asistencia
		  (format t "~%    -*Actualizacion de Datos Asistencia*-~%~%")

		  (format t "~%Listado  de Asistencia")
		  (format t "~%~%Num_Clase ~20TAsistio")
		  (print_asistencia (select_asistencia_idMateria r_codigo) Asistencias)
		  (format t "Ingrese Num Clase: ")
		  (setq num_clase (read))
		  
		  (format t "~%~%Num_Clase ~20TAsistio")
		  (print_asistencia (select_asistencia (where_asistencia :IDMateria r_codigo :Num_Clase num_clase )) Asistencias)
		  
		  (update_asistencia (where_asistencia :IDMateria r_codigo :Num_Clase num_clase) :isAsistio (y-or-n-p "isAsistio [y/n]: "))
		  (format t "~%Asistencia modificada con �xito")		
		 )

		 ((4);Eliminar
		  (format t "~%    -*Eliminacion de Asistencia*-~%~%")

		 )
	       )

	   while (< op1 4))
	  (setq op 0)
	  )

	 ((3);____TAREAS_____-
	  ;Al ingresar en esta parte del men�, se pide de antemano la materia de la cual se desea realizar acciones con la Tareas.
	  (setq r_codigo 0)
	  (print_listado_materia)
	  (format t "~%Seleccione materia (codigo): ")
	  (setq r_codigo (read))
	  (format t "Codigo~20TNombre~50THoras")
	  (print_materia (select_materia_codigo r_codigo)  Materias)
	  
	  (loop do 
	       (format t "~%    -*Tareas*-~%~%")
	       (format t "  1) Consultar listado~%")
	       (format t "  2) Nuevo~%")
	       (format t "  3) Editar~%")
	       (format t "  4) Eliminar~%")
	       (format t "  5) Regresar~%")
	       (format t "~%Seleccione una opcion: ")
	       (setq op1 (read))
	       (terpri)
	       
	       (case op1
		 ((1) ;Consultar Tareas 
		  (format t "~%~%Num_Tarea ~20TNota")
		  (setq Tareas *Db_Tareas*)
		  (print_tarea (select_tarea_idMateria r_codigo) Tareas)		  
		 )
		 
		  ((2) ;Nueva Tarea		  
		  (format t "~%   -*Nueva Tarea*-~%")
		  (add_listado_tarea_idMat r_codigo )
		  (save_Db_tarea "Tareas.db")
		 )
		 ((3);Actualizacion de Tarea
		  (format t "~%    -*Actualizacion de Datos Tarea*-~%~%")

		  (format t "~%Listado  de Tarea")
		  (format t "~%~%Num_Tarea ~20TNota")
		  (print_tarea (select_tarea_idMateria r_codigo) Tareas)
		  (format t "Ingrese Num_Tarea: ")
		  (setq num_tarea (read))
		  
		  (format t "~%~%Num_Clase ~20TAsistio")
		  (print_tarea (select_tarea (where_tarea :IDMateria r_codigo :Num_tarea num_tarea )) Tareas)
		  
		   (format t "Cumplio?: ")
		   (setq nota (read))
		  (update_tarea (where_tarea :IDMateria r_codigo :Num_Tarea num_tarea) :isCumplio nota)
		  (format t "~%Tarea modificada con �xito")		
		 )

		 ((4);Eliminar
		  (format t "~%    -*Eliminacion de tarea*-~%~%")

		  (format t "~%Listado  de Tarea")
		  (format t "~%~%Num_Tarea ~20TNota")
		  (print_tarea (select_tarea_idMateria r_codigo) Tareas)
		  (format t "Ingrese Num_Tarea: ")
		  (setq num_tarea (read))
		  
		  (format t "~%~%Num_Clase ~20TAsistio")
		  (print_tarea (select_tarea (where_tarea :IDMateria r_codigo :Num_tarea num_tarea )) Tareas)
		  (remove_tarea (where_tarea :IDMateria r_codigo :Num_tarea num_tarea))
		  (format t "~%Tarea eliminada con �xito")

		 )
	       )

	   while (< op1 4))
	  (setq op 0)
	  )

	 ((4);____Proyectos_____
	  ;Al ingresar en esta parte del men�, se pide de antemano la materia de la cual se desea realizar acciones con la Proyectos.
	  (setq r_codigo 0)
	  (print_listado_materia)
	  (format t "~%Seleccione materia (codigo): ")
	  (setq r_codigo (read))
	  (format t "Codigo~20TNombre~50THoras")
	  (print_materia (select_materia_codigo r_codigo)  Materias)
	  
	  (loop do 
	       (format t "~%    -*Proyectos*-~%~%")
	       (format t "  1) Consultar listado~%")
	       (format t "  2) Nuevo~%")
	       (format t "  3) Editar~%")
	       (format t "  4) Eliminar~%")
	       (format t "  5) Regresar~%")
	       (format t "~%Seleccione una opcion: ")
	       (setq op1 (read))
	       (terpri)
	       
	       (case op1
		 ((1) ;Consultar Proyectos 
		  (format t "~%~%Num_proyecto ~20TNota")
		  (setq Proyectos *Db_Proyectos*)
		  (print_proyecto (select_proyecto_idMateria r_codigo) Proyectos)		  
		 )
		 
		  ((2) ;Nueva proyecto		  
		  (format t "~%   -*Nueva proyecto*-~%")
		  (add_listado_proyecto_idMat r_codigo )
		  (save_Db_proyecto "Proyectos.db")
		 )
		 ((3);Actualizacion de proyecto
		  (format t "~%    -*Actualizacion de Datos proyecto*-~%~%")

		  (format t "~%Listado  de proyecto")
		  (format t "~%~%Num_proyecto ~20TNota")
		  (print_proyecto (select_proyecto_idMateria r_codigo) Proyectos)
		  (format t "Ingrese Num_proyecto: ")
		  (setq num_proyecto (read))
		  
		   (format t "~%~%Num_proyecto ~20TNota")
		  (print_proyecto (select_proyecto (where_proyecto :IDMateria r_codigo :Num_proyecto num_proyecto )) Proyectos)
		  
		   (format t "Ingrese nota: ")
		   (setq nota (read))
		  (update_proyecto (where_proyecto :IDMateria r_codigo :Num_proyecto num_proyecto) :nota nota)
		  (format t "~%proyecto modificada con �xito")		
		 )

		 ((4);Eliminar
		  (format t "~%    -*Eliminacion de proyecto*-~%~%")

		  (format t "~%Listado  de proyecto")
		  (format t "~%~%Num_proyecto ~20TNota")
		  (print_proyecto (select_proyecto_idMateria r_codigo) Proyectos)
		  (format t "Ingrese Num_proyecto: ")
		  (setq num_proyecto (read))
		  
		  (format t "~%~%Num_proyecto ~20TNota")
		  (print_proyecto (select_proyecto (where_proyecto :IDMateria r_codigo :Num_proyecto num_proyecto )) Proyectos)
		  (remove_proyecto (where_proyecto :IDMateria r_codigo :Num_proyecto num_proyecto))
		  (format t "~%proyecto eliminada con �xito")

		 )
	       )

	   while (< op1 4))
	  (setq op 0)
	  )

	 ((5);____EXAMENES_____-
	  ;Al ingresar en esta parte del men�, se pide de antemano la materia de la cual se desea realizar acciones con la examenes.
	  (setq r_codigo 0)
	  (print_listado_materia)
	  (format t "~%Seleccione materia (codigo): ")
	  (setq r_codigo (read))
	  (format t "Codigo~20TNombre~50THoras")
	  (print_materia (select_materia_codigo r_codigo)  Materias)
	  
	  (loop do 
	       (format t "~%    -*EXAMENES*-~%~%")
	       (format t "  1) Consultar listado~%")
	       (format t "  2) Nuevo~%")
	       (format t "  3) Editar~%")
	       (format t "  4) Eliminar~%")
	       (format t "  5) Regresar~%")
	       (format t "~%Seleccione una opcion: ")
	       (setq op1 (read))
	       (terpri)
	       
	       (case op1
		 ((1) ;Consultar Examenes 
		  (format t "~%~%Num_Examen ~20TNota")
		  (setq Examenes *Db_Examenes*)
		  (print_examen (select_examen_idMateria r_codigo) Examenes)		  
		 )
		 
		  ((2) ;Nueva examen		  
		  (format t "~%   -*Nueva Examen*-~%")
		  (add_listado_examen_idMat r_codigo )
		  (save_Db_examen "Examenes.db")
		 )
		 ((3);Actualizacion de examen
		  (format t "~%    -*Actualizacion de Datos Examen*-~%~%")

		  (format t "~%Listado  de examen")
		  (format t "~%~%Num_Examen ~20TNota")
		  (print_examen (select_examen_idMateria r_codigo) Examenes)
		  (format t "Ingrese Num_examen: ")
		  (setq num_examen (read))
		  
		  (format t "~%~%Num_Examen ~20TNota")
		  (print_examen (select_examen (where_examen :IDMateria r_codigo :Num_examen num_examen )) Examenes)
		  
		   (format t "Ingrese nota: ")
		   (setq nota (read))
		  (update_examen (where_examen :IDMateria r_codigo :Num_examen num_examen) :nota nota)
		  (format t "~%Examen modificada con �xito")		
		 )

		 ((4);Eliminar
		  (format t "~%    -*Eliminacion de examen*-~%~%")

		  (format t "~%Listado  de examen")
		  (format t "~%~%Num_examen ~20TNota")
		  (print_examen (select_examen_idMateria r_codigo) Examenes)
		  (format t "Ingrese Num_examen: ")
		  (setq num_examen (read))
		  
		  (format t "~%~%Num_Examen ~20TNota")
		  (print_examen (select_examen (where_examen :IDMateria r_codigo :Num_examen num_examen )) Examenes)
		  (remove_examen (where_examen :IDMateria r_codigo :Num_examen num_examen))
		  (format t "~%examen eliminada con �xito")

		 )
	       )

	   while (< op1 4))
	  (setq op 0)
	  )
	((6)
	 (setq r_codigo 0)
	 (print_listado_materia)
	 (format t "~%Seleccione materia (codigo): ")
	 (setq r_codigo (read))
	 (setq Materias *Db_Materias*)
	 (setq Asistencias *Db_Asistencias*)
	 (setq Examenes *Db_Examenes*)
	 (setq Tareas *Db_Tareas*)
	 (setq Proyectos *Db_Proyectos*)
	 
	 (format t "~%~10T-Porcentaje de asistencia:  ~2,F" 
		 (* (/
		     (sublistas (select_Asistencia (where_asistencia :IDMateria r_codigo :isAsistio T )))
		     (sublistas (select_Asistencia (where_asistencia :IDMateria r_codigo))))
		    100)
		 )
	 
	 (format t "~%~10T-Porcentaje en ex�menes: ~,2F" 
		 (float (*(/
			      (sumar_val_lista (select_Examen (where_examen :iDMateria r_codigo)) 5)
			      (*(sublistas (select_Examen (where_examen :iDMateria r_codigo)))
			     100)) 100))
		 )
	 
	 (format t "~%~10T-Porcentaje en tareas: ~,2F" 
		 (float (* (/
			    (sublistas (select_Tarea (where_tarea :iDMateria r_codigo :isCumplio 1)))
			    (sublistas (select_Tarea (where_tarea :iDMateria r_codigo ))))
			   100))
		 )
	 
	(format t "~%~10T-Porcentaje en proyectos: ~,2F" 
		 (float (*(/
			      (sumar_val_lista (select_Proyecto (where_proyecto :iDMateria r_codigo)) 5)
			      (*(sublistas (select_Proyecto (where_proyecto :iDMateria r_codigo)))
			     100)) 100))
	)
	
	(format t "~%~10T-Porcentaje en global: ~,2F" 
		(/ (+
		;Ponderacion examenes
		(float (*(*(/
			      (sumar_val_lista (select_Examen (where_examen :iDMateria r_codigo)) 5)
			      (*(sublistas (select_Examen (where_examen :iDMateria r_codigo)))
			     100)) 100) (/ (nth 9 (nth 0 (select_materia (where_materia :codigo r_codigo)))) 100)))
		
		;ponderacion proyectos
		(float (* (*(/
			      (sumar_val_lista (select_Proyecto (where_proyecto :iDMateria r_codigo)) 5)
			      (*(sublistas (select_Proyecto (where_proyecto :iDMateria r_codigo)))
			     100)) 100) (/ (nth 11 (nth 0 (select_materia (where_materia :codigo r_codigo)))) 100)))
		;ponderacion tareas		 
		(float (* (* (/
			    (sublistas (select_Tarea (where_tarea :iDMateria r_codigo :isCumplio 1)))
			    (sublistas (select_Tarea (where_tarea :iDMateria r_codigo ))))
			   100) (/ (nth 7 (nth 0 (select_materia (where_materia :codigo r_codigo)))) 100)))
	 ) 3)
	 )
	)
	)
	while(< op 7)))

