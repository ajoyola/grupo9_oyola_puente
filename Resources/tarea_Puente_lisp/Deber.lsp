;;;; Autora: Sianna Puente
;;;; Para compilacion digitar en consola (load "deber.lsp")
;;;; Luego llamar la funcion programa, digitando (programa)

(defun programa()
	(loop do
		(system "CLS")
		;INICIO
		(format t "~%~%                           *_*_*_BIENVENIDO_*_*_*~%")
		(format t "  MENU ~%")
		(format t "    1) Registrar nuevo usuario~%")
		(format t "    2) Lista de usuarios~%")
		(format t "    3) Salir~%")

		(format t "~%Seleccione una opcion: ")
		(setq op (read))
		(terpri)
		(case op 	
			
			((1)
				(system "CLS")
				(format t "~%    -*Nuevo Usuario*-~%~%")
				;Pedir campos Nombre, e email
				(princ "Ingrese el nombre: ")
				(setq nombre (read-line))
				(princ "Ingrese el email:  ")
				(setq email (read-line))
				(terpri)
				
				;Guardar en el archivo los datos del usuario y su email
				(with-open-file (stream "Registro.txt" :direction :output :if-exists :append :if-does-not-exist :create)
				   (format stream nombre)
				   (format stream "~20T")
				   (format stream email)
				   (format stream "~%")
				   (format t "~% Usuario registrado con exito")
				   (setq bre_ak (read-line))
				)
				
			
			)
			
			((2)
				(system "CLS")
				(format t "           *REGISTRO* ~%")
				(format t "~%Usuario~20TEmail~%-------~20T-----~%~%")				
				
				;Leer el archivo "Registro.txt" y mostrarlo en pantalla				
				(let ((in (open "Registro.txt" :if-does-not-exist nil)))
				   (when in
					  (loop for line = (read-line in nil)				  
						while line do (format t "~a~%" line))
					  (close in)
				   )
				)
				
				(format t "~%----------------------- ~%Aceptar")
				(setq bre_ak (read-line))
			)
			
			((3)
				(format t "~%Gracias por acceder al sistema")
				(setq bre_ak (read-line))
			)
			
			(otherwise 
				(format t "~%Opcion incorrecta. Ingrese nuevamente.~%")
				(setq bre_ak (read-line))
				(setq op 0)
			)
		)
	while (< op 3))
)

